# AWS ECS

## Amazon ECS
- Clusters
- Create Cluster
	- cluster template > EC2 Linux + Networking
		- ! Auto assign public IP = Enabled
	- Launch status > 3x green
- View Cluster
	- status = active
	- waiting for instance to start... > ok
	- check instances > get IP Addr
- Create New Task definition
	- EC2
	- define Memory and CPU
	- Add container
		- get GitLab Imaga path > registry.gitlab.com/ser-cal/docker-bootstrap/webserver_one:1.0
		- Port mappings > 80 - 8080 (make sure port is defined in firewall)
		- Add
	- Create > created successfully
- Clusters > Tasks > run new task
	- Launch type > EC2
	- Cluster > my new cluster
	- Run task
- Check Site in Browser > http://44.211.218.129/

This is it!